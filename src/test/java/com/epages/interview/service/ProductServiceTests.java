package com.epages.interview.service;

import com.epages.interview.entity.Product;
import com.epages.interview.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class ProductServiceTests {

    @Mock
    private ProductRepository productRepository;

    @Test
    public void getAll_returnAllProducts() {
        var product1 = new Product().withId(1L).withName("name1").withPrice(12.3).withBrand("B").withOnSale(true);
        var product2 = new Product().withId(1L).withName("name2").withPrice(12.3).withBrand("A").withOnSale(true);

        Mockito.when(productRepository.findAll()).thenReturn(List.of(product1, product2));

        var productService = new ProductService(productRepository);
        var result = productService.getAll();

        assertThat(result).isNotEmpty();
        assertThat(result.entrySet().size()).isEqualTo(2);
    }

    @Test
    public void getAll_returnSortedBrands() {
        var brand1 = "A";
        var brand2 = "B";

        var product1 = new Product().withId(1L).withName("name1").withPrice(12.3).withBrand(brand2).withOnSale(true);
        var product2 = new Product().withId(1L).withName("name2").withPrice(12.3).withBrand(brand1).withOnSale(true);

        Mockito.when(productRepository.findAll()).thenReturn(List.of(product1, product2));

        var productService = new ProductService(productRepository);
        var result = productService.getAll();

        assertThat(result).isNotEmpty();
        var iterator = result.entrySet().iterator();
        assertThat(iterator.next().getKey()).isEqualTo(brand1);
        assertThat(iterator.next().getKey()).isEqualTo(brand2);
    }

    @Test
    public void getAll_returnSortedProducts() {
        var price1 = 12.3;
        var brand = "A";
        var product1 = new Product().withId(1L).withName("name1").withPrice(price1).withBrand(brand).withOnSale(true);
        var price2 = 1.1;
        var product2 = new Product().withId(1L).withName("name2").withPrice(price2).withBrand(brand).withOnSale(true);

        Mockito.when(productRepository.findAll()).thenReturn(List.of(product1, product2));

        var productService = new ProductService(productRepository);
        var result = productService.getAll();

        assertThat(result).isNotEmpty();
        assertThat(result.get(brand).get(0).getPrice()).isEqualTo(price2);
        assertThat(result.get(brand).get(1).getPrice()).isEqualTo(price1);
    }
}
