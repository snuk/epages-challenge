package com.epages.interview.service;

import com.epages.interview.entity.Product;
import com.epages.interview.model.ProductModel;
import com.epages.interview.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.*;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Map<String, List<ProductModel>> getAll() {
        var products = productRepository.findAll();

        return products.stream()
                .sorted(comparing(Product::getBrand).thenComparing(Product::getPrice))
                .collect(groupingBy(Product::getBrand, LinkedHashMap::new, mapping(ProductModel::new, toList())));
    }
}
