package com.epages.interview.model;

import com.epages.interview.entity.Product;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("product")
public class ProductModel {

    @ApiModelProperty("product identifier")
    private Long id;

    @ApiModelProperty("product name")
    private String name;

    @ApiModelProperty("product price")
    private Double price;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty("product event")
    private ProductEvent event;

    public ProductModel(Product product) {
        this.id = product.getId();
        this.name = product.getName();
        this.price = product.getPrice();
        this.event = product.isOnSale() ? ProductEvent.ON_SALE : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public ProductEvent getEvent() {
        return event;
    }

    public void setEvent(ProductEvent event) {
        this.event = event;
    }
}
