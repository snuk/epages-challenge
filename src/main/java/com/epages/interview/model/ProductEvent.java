package com.epages.interview.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum ProductEvent {
    @JsonProperty("ON SALE")
    ON_SALE
}
