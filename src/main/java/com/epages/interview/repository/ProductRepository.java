package com.epages.interview.repository;

import com.epages.interview.entity.Product;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ProductRepository extends MongoRepository<Product, Long> {

    public List<Product> findAll();
}
