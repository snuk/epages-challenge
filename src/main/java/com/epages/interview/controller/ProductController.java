package com.epages.interview.controller;

import com.epages.interview.model.ProductModel;
import com.epages.interview.service.ProductService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/products")
    @ApiOperation("get all products")
    Map<String, List<ProductModel>> all() {
        return productService.getAll();
    }
}
